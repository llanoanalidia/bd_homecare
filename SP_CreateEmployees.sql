USE [HomeCare]
GO
/****** Object:  StoredProcedure [dbo].[Insert_Employee]    Script Date: 3/19/2021 7:37:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[Insert_Employee]
	-- Add the parameters for the stored procedure here
	@UserName nvarchar(120),
	@Pasword nvarchar(120),
	@RoleId int,
	@State nvarchar (20), 
	@FirstName nvarchar (200), 
	@Middle nvarchar (10),
	@LastName nvarchar(100), 
	@DateOfBirth date, 
	@SocialSecurityNumber nvarchar(100), 
	@Gender nvarchar (20), 
	@Phone1 nvarchar (40),
	@Phone2 nvarchar(40), 
	@eMail nvarchar(40),
	@Status nvarchar(100),
	@Adress1 nvarchar (100),
	@Adress2 nvarchar (100), 
	@City nvarchar (50), 
	@StateC nvarchar (50),
	@ZipCode nvarchar (50)
AS
BEGIN

	INSERT INTO [dbo].[User] (UserName,Pasword,RoleId,State)
	VALUES (@UserName,@Pasword,@RoleId,@State)

	DECLARE @UserId INT = SCOPE_IDENTITY();
	

	INSERT INTO [dbo].[Employee] (FirstName,Middle,LastName,DateOfBirth,
								 SocialSecurityNumber,Gender,Phone1,Phone2,
								 eMail,Status,IdRole,Address1,Address2,City,State,ZipCode,IdUser)
	VALUES (@FirstName,@Middle,@LastName,@DateOfBirth,
			@SocialSecurityNumber,@Gender,@Phone1,@Phone2, 
			@eMail,@Status,@RoleId,@Adress1,@Adress2,@City,@StateC,@ZipCode,@UserId)

END
